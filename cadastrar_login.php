<?php
//Verificando se houve erro no cadastro do login
if(isset($_GET['erro'])==1){
  $erro=1;
}else{
  $erro=0;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Jessé Levandovski"> 
        <title>Dashboard My System</title> 
        <!-- Bootstrap core CSS -->
        <link href="bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- CSS personalizado -->
        <link href="bootstrap-3.3.5-dist/css/signin.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
        <link href="css/estilo.css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery.js"></script> 

        <div class="container login">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title h1">Cadastrar Login</h3>
                </div>
                <br >
                <form name="cadastro" class="form-horizontal" action="dao/cadastrando_login.php" method="post">
                    <fieldset>
                    <div class="form-group">
                            <label for="inputNome" class="col-lg-2 control-label">User</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control" id="inputNome" name="nome" placeholder="Insira seu nome de usuário" value="" required="">
                            </div>
                        </div> 
                       <div class="form-group">
                            <label for="inputNome" class="col-lg-2 control-label">E-mail</label>
                            <div class="col-lg-9">
                                <input type="email" class="form-control" id="inputNome" name="email" placeholder="Insira seu email" value="" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputNome" class="col-lg-2 control-label">Senha</label>
                            <div class="col-lg-9">
                                <input type="password" class="form-control" id="inputNome" name="senha" placeholder="Insira sua senha" value="" required="">
                            </div>
                        </div>
                             
                            <div class="form-group">
                            <div class="col-lg-6 col-lg-offset-2">
                                <button type="submit" name="btnlogin" class="btn btn-success" value="login">Cadastrar</button>
                            </div>
                            </div>
                            
                            
                            <?php if($erro==1){?>
                            <div class="form-group">
                            <label for="inputNome" class="col-lg-2 control-label"></label>
                            <div class="col-lg-9">
                                <span class="erro">Você já possui cadastro!!! <a href="index.php">Voltar</a></span>
                            </div>
                            </div>
                            <?php } ?>
                    </fieldset>
                </form>        
            </div> <!-- /container -->
 
        </div>
        <hr>
        <footer>
            <p class="text-center">© Desenvolvido by: Jessé Levandovski</p>
        </footer>
 
        <!-- Scripts jQuery e Bootstrap -->
        <script src="bootstrap-3.3.5-dist/js/jquery-1.11.3.min.js"></script>
        <script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    </body>
</html>           
    </head>
    <body>
    