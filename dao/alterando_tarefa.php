<?php
require_once "conexao.php";
$tarefa=isset($_POST['tarefa'])?$_POST['tarefa']:"";
$descricao=isset($_POST['descricao'])?$_POST['descricao']:"";
$prioridade=isset($_POST['prioridade'])?$_POST['prioridade']:"";
$inserida=isset($_POST['usuario'])?$_POST['usuario']:"";
$id=isset($_POST['id'])?$_POST['id']:0;
//verificando se o id da tela atualizar_tarefa esta vazio
if($id!=0){
    $stmt=$conn->prepare("UPDATE task SET tarefa=?,descricao=?,prioridade=?,inserida=? WHERE id=?");
    $stmt->bindParam(1,$tarefa);
    $stmt->bindParam(2,$descricao);
    $stmt->bindParam(3,$prioridade);
    $stmt->bindParam(4,$inserida);
    $stmt->bindParam(5,$id);
    $stmt->execute();
    header('Location: ../listar_tarefa.php');
}else{
    header("Location: ../altualizar_tarefa.php");
}
?>