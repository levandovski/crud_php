<?php
require_once "conexao.php";
session_start();
$finalizada=$_SESSION['user'];
$status="Finalizada";
if(isset($_GET['id'])){
    $id=isset($_GET['id'])?$_GET['id']:0;
    //Verificando se o Id esta vazio
    if($id!=0){
        $stmt=$conn->prepare("UPDATE task SET finalizada=?,status=? where id=?");
        $stmt->bindParam(1,$finalizada);
        $stmt->bindParam(2,$status);
        $stmt->bindParam(3,$id);
        $stmt->execute();
        header("Location: ../listar_tarefa.php");
        }else{
        header("Location: ../listar_tarefa.php");
        }
    }
?>