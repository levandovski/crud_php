<?php
require_once "conexao.php"; 
session_start();
$nome=isset($_POST['nome'])?$_POST['nome']:"";
$descricao=isset($_POST['descricao'])?$_POST['descricao']:"";
$prioridade=isset($_POST['prioridade'])?$_POST['prioridade']:"";
$status="Processada";
$inserida=$_SESSION['user'];
$finalizada="";

//Realizando cadastro da tarefa no banco de dados
$stmt=$conn->prepare("INSERT INTO task (tarefa,descricao,prioridade,status,inserida,finalizada) VALUES (?,?,?,?,?,?)");
$stmt->bindParam(1,$nome);
$stmt->bindParam(2,$descricao);
$stmt->bindParam(3,$prioridade);
$stmt->bindParam(4,$status);
$stmt->bindParam(5,$inserida);
$stmt->bindParam(6,$finalizada);
$stmt->execute();
header("Location: ../listar_tarefa.php");
?>